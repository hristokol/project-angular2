'use strict'

var express=require('express');
var https=require('https');
var http=require('http');
var fs=require('fs');

//https options
/*var options={
    key: fs.readFileSync(__dirname + '/key.pem', 'utf8'),
    cert: fs.readFileSync(__dirname + '/cert.pem', 'utf8'),
    requestCert: true,
    rejectUnauthorized: false
};*/

var app=express();
app.use(express.static(__dirname+'/../build'));

var httpServer=http.createServer(app);
//var httpsServer=https.createServer(options,app);

httpServer.listen(3000);
//httpsServer.listen(443);
