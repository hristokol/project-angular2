var gulp = require("gulp");
var del = require("del");
var tsc = require("gulp-typescript");
var sourcemaps = require('gulp-sourcemaps');
var tsProject = tsc.createProject("tsconfig.json");
var sass = require('gulp-sass');
//var tslint = require('gulp-tslint');


/**
 * Remove build directory.
 */
gulp.task('clean', function (cb) {
    return del(["build"], cb);
});

/**
 * Compile TypeScript sources and create sourcemaps in build directory.
 */
gulp.task("compile", function () {
    var tsResult = gulp.src("app/**/*.ts")
        //.pipe(sourcemaps.init())
        .pipe(tsc(tsProject));
    return tsResult.js
        //.pipe(sourcemaps.write("."))
        .pipe(gulp.dest("build/src"));
});

gulp.task("templates", function () {
    return gulp.src(["app/**/*", "!**/*.ts"])
        .pipe(gulp.dest("build/src"))
});
/**
 * Copy all resources that are not TypeScript files into build directory.
 */
gulp.task("resources", function () {
    return gulp.src(["assets/**/*", "!**/*.sass", "assets/**/*.svg", "assets/**/*.png", "assets/**/*.gif"])
        .pipe(gulp.dest("build/css"))
});

gulp.task("styles", function () {
    return gulp.src(["assets/**/*", "!**/*.ts", "!**/*.svg", "!**/*.png", "!**/*.gif"])
        .pipe(sass())
        .pipe(gulp.dest("build/css"))
});

gulp.task("pages", function () {
    return gulp.src("index.html")
        .pipe(gulp.dest("build"))
});

gulp.task("systemjs", function () {
    return gulp.src("systemjs.config.js")
        .pipe(gulp.dest("build"))
});

gulp.task("external-libs", function () {
    return gulp.src("libs/**/*.js")
        .pipe(gulp.dest("build/libs"))
});

/**
 * Copy all required libraries into build directory.
 */
gulp.task("libs", function () {
    return gulp.src([
            'es6-shim/es6-shim.min.js',
            'systemjs/dist/system-polyfills.js',
            'systemjs/dist/system.src.js',
            'reflect-metadata/Reflect.js',
            'rxjs/**',
            'zone.js/dist/**',
            '@angular/**',
            'socket.io-client/socket.io.js',
            'fastclick/lib/fastclick.js'
        ], {cwd: "node_modules/**"}) /* Glob required here. */
        .pipe(gulp.dest("build/node_modules"));
});

/**
 * Build the project.
 */
gulp.task("build", ['libs','compile', 'templates', 'pages', 'external-libs', 'systemjs', 'resources', 'styles'], function () {
    console.log("Building the project ...")
});