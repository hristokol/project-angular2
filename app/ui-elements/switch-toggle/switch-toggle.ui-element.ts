import {Component, Input} from '@angular/core';

@Component({
    selector: 'switch-toggle',
    templateUrl: 'app/ui-elements/switch-toggle/views/switch-toggle.html',
    styleUrls: ['dist/build/ui-elements/switch-toggle/switch-toggle.css']
})

export class SwitchToggle {
    @Input('checked') isChecked:boolean;

    constructor() {}

    public toggle() {
        this.isChecked = !this.isChecked;
    }

}