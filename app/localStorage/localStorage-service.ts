import {Injectable} from '@angular/core';

@Injectable()
export class LocalStorageService {
    constructor() {

    }

    public getItem(key, default_value?) {
        default_value = default_value == undefined ? null : default_value;

        try {
            var item = JSON.parse(localStorage.getItem(key));
        } catch (e) {
            item = default_value;
        }

        if (item === null) {
            item = default_value;
        }

        return item;
    }

    public setItem(key, data) {
        localStorage.setItem(key, JSON.stringify(data));
    }

    public removeItem(key) {
        localStorage.removeItem(key);
    }

    public hasItem(key) {
        return key in localStorage;
    }
}
