import {Injectable, Inject} from '@angular/core';
import {LocalStorageService} from "../localStorage/localStorage-service";
import ELanguage = Options.ELanguage;

@Injectable()
export class OptionService {
    private soundEnabled:boolean;
    private language:ELanguage;
    private localStorageService:LocalStorageService;

    constructor(localStorageService:LocalStorageService) {
        this.localStorageService = localStorageService;
        this.loadOptions();
    }

    private loadOptions() {
        this.soundEnabled = this.localStorageService.getItem('soundEnabled', true);
        this.language = this.localStorageService.getItem('language', 'bg');
    }

    public getSoundEnabled():boolean {
        return this.soundEnabled;
    }

    public getLanguage():ELanguage {
        return this.language;
    }

    public setSoundEnabled(value:boolean) {
        this.soundEnabled = value;
        this.localStorageService.setItem('soundEnabled', value);
    }

    public setLanguage(value:ELanguage) {
        this.language = value;
        this.localStorageService.setItem('language', value);
    }
}