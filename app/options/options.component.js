"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var options_service_1 = require("./options-service");
var switch_toggle_ui_element_1 = require("../ui-elements/switch-toggle/switch-toggle.ui-element");
var localStorage_service_1 = require("../localStorage/localStorage-service");
var OptionsCmp = (function () {
    function OptionsCmp(optionsService) {
        this.optionsService = optionsService;
    }
    OptionsCmp.prototype.ngOnInit = function () {
        this.soundEnabled = this.optionsService.getSoundEnabled();
        this.selectedLanguage = this.optionsService.getLanguage();
        this.languages = ['BG', 'EN'];
        this.listOpened = false;
    };
    OptionsCmp.prototype.toggleSoundEnabled = function () {
        this.soundEnabled = !this.soundEnabled;
        this.optionsService.setSoundEnabled(this.soundEnabled);
    };
    OptionsCmp.prototype.setList = function (value) {
        this.listOpened = value;
    };
    OptionsCmp.prototype.toggleList = function () {
        this.listOpened = !this.listOpened;
    };
    OptionsCmp.prototype.setLanguage = function (language) {
        this.selectedLanguage = language;
        this.optionsService.setLanguage(language);
    };
    OptionsCmp.prototype.back = function () {
        this.backCb();
    };
    __decorate([
        core_1.Input('back'), 
        __metadata('design:type', Function)
    ], OptionsCmp.prototype, "backCb", void 0);
    OptionsCmp = __decorate([
        core_1.Component({
            selector: 'settings',
            templateUrl: 'app/options/views/options.html',
            styleUrls: ['dist/build/options/options.css'],
            providers: [localStorage_service_1.LocalStorageService, options_service_1.OptionService],
            directives: [switch_toggle_ui_element_1.SwitchToggle]
        }), 
        __metadata('design:paramtypes', [options_service_1.OptionService])
    ], OptionsCmp);
    return OptionsCmp;
}());
exports.OptionsCmp = OptionsCmp;
//# sourceMappingURL=options.component.js.map