import {Component,Input, OnInit} from '@angular/core';
import {OptionService} from "./options-service";
import ELanguage = Options.ELanguage;
import {SwitchToggle} from "../ui-elements/switch-toggle/switch-toggle.ui-element";
import {LocalStorageService} from "../localStorage/localStorage-service";

@Component({
    selector: 'settings', //<settings></settings>,
    templateUrl: 'app/options/views/options.html',
    styleUrls: ['dist/build/options/options.css'],
    providers: [LocalStorageService, OptionService],
    directives: [SwitchToggle]
})

export class OptionsCmp implements OnInit {
    @Input('back') backCb:Function;
    public soundEnabled:boolean;
    public languages:string[];
    public listOpened:boolean;
    private selectedLanguage:ELanguage;
    private optionsService:OptionService;

    constructor(optionsService:OptionService) {
        this.optionsService = optionsService;
    }

    public ngOnInit() {
        this.soundEnabled = this.optionsService.getSoundEnabled();
        this.selectedLanguage = this.optionsService.getLanguage();
        this.languages = ['BG', 'EN'];
        this.listOpened = false;
    }

    public toggleSoundEnabled() {
        this.soundEnabled = !this.soundEnabled;
        this.optionsService.setSoundEnabled(this.soundEnabled);
    }

    public setList(value:boolean) {
        this.listOpened = value;
    }

    public toggleList() {
        this.listOpened = !this.listOpened;
    }

    public setLanguage(language:ELanguage) {
        this.selectedLanguage = language;
        this.optionsService.setLanguage(language);
    }

    public back() {
        this.backCb();
    }

}