"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var localStorage_service_1 = require("../localStorage/localStorage-service");
var OptionService = (function () {
    function OptionService(localStorageService) {
        this.localStorageService = localStorageService;
        this.loadOptions();
    }
    OptionService.prototype.loadOptions = function () {
        this.soundEnabled = this.localStorageService.getItem('soundEnabled', true);
        this.language = this.localStorageService.getItem('language', 'bg');
    };
    OptionService.prototype.getSoundEnabled = function () {
        return this.soundEnabled;
    };
    OptionService.prototype.getLanguage = function () {
        return this.language;
    };
    OptionService.prototype.setSoundEnabled = function (value) {
        this.soundEnabled = value;
        this.localStorageService.setItem('soundEnabled', value);
    };
    OptionService.prototype.setLanguage = function (value) {
        this.language = value;
        this.localStorageService.setItem('language', value);
    };
    OptionService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [localStorage_service_1.LocalStorageService])
    ], OptionService);
    return OptionService;
}());
exports.OptionService = OptionService;
//# sourceMappingURL=options-service.js.map