'use strict';
var Options;
(function (Options) {
    (function (ELanguage) {
        ELanguage[ELanguage["BG"] = 'bg'] = "BG";
        ELanguage[ELanguage["EN"] = 'en'] = "EN";
    })(Options.ELanguage || (Options.ELanguage = {}));
    var ELanguage = Options.ELanguage;
})(Options || (Options = {}));
//# sourceMappingURL=options-types.js.map