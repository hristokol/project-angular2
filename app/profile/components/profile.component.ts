import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES,RouterOutlet} from '@angular/router-deprecated';
import {ProfileService} from "./profile-service";

@Component({
    selector: 'user-profile',
    providers: [ProfileService],
    directives: [ROUTER_DIRECTIVES,RouterOutlet],
    templateUrl: 'app/profile/components/views/profile.html',
    styleUrls: ['dist/build/profile/profile.css']
})

export class ProfileCmp implements OnInit {

    private _profileService:ProfileService;
    public profile:any;

    constructor(profileService:ProfileService) {
        this._profileService = profileService;
    }

    ngOnInit():any {
        this._load();
    }

    private _load() {
        this.profile = this._profileService.loadMock();
    }

}