import {Injectable} from '@angular/core';
import {Http,Response} from '@angular/http';

@Injectable()
export class ProfileService {

    constructor() {

    }

    public loadMock() {
        return {
            id: 'fjkhgjfkhgfldkjdfd',
            avatarUrl: "https://scontent-vie1-1.xx.fbcdn.net/v/t1.0-9/13118962_987362174684686_2846474144423139514_n.jpg?oh=8e79d34adfcb8ce5cbc8fb45c541c63e&oe=580C28EE",
            email: 'lol@mailinator.com',
            age: '23',
            contacts: '120',
            firstName: 'Hristo',
            lastName: 'Kolev',
            workplace: 'Casualino JSC',
            job: 'Front-end developer',
            interests: ['Sport', 'Programming', 'Films', 'Games']
        }
    }

    public getUserProfileMock(userId:string){
        return {
            id: 'fjkhgjfkhgfldkjdfd',
            avatarUrl: "https://scontent-vie1-1.xx.fbcdn.net/v/t1.0-9/13118962_987362174684686_2846474144423139514_n.jpg?oh=8e79d34adfcb8ce5cbc8fb45c541c63e&oe=580C28EE",
            email: 'lol@mailinator.com',
            age: '23',
            contacts: '120',
            firstName: 'Hristo',
            lastName: 'Kolev',
            workplace: 'Casualino JSC',
            job: 'Front-end developer',
            interests: ['Sport', 'Programming', 'Films', 'Games']
        }
    }

}