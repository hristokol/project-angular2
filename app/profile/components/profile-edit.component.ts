import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES,RouterOutlet} from '@angular/router-deprecated';
import {ProfileService} from "./profile-service";

@Component({
    selector: 'profile-edit',
    providers: [ProfileService],
    directives: [ROUTER_DIRECTIVES, RouterOutlet],
    templateUrl: 'app/profile/components/views/editProfile.html',
    styleUrls: ['dist/build/profile/editProfile.css']
})

export class EditProfileCmp {

    constructor() {

    }

    public edit(){

    }

}