"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ProfileService = (function () {
    function ProfileService() {
    }
    ProfileService.prototype.loadMock = function () {
        return {
            id: 'fjkhgjfkhgfldkjdfd',
            avatarUrl: "https://scontent-vie1-1.xx.fbcdn.net/v/t1.0-9/13118962_987362174684686_2846474144423139514_n.jpg?oh=8e79d34adfcb8ce5cbc8fb45c541c63e&oe=580C28EE",
            email: 'lol@mailinator.com',
            age: '23',
            contacts: '120',
            firstName: 'Hristo',
            lastName: 'Kolev',
            workplace: 'Casualino JSC',
            job: 'Front-end developer',
            interests: ['Sport', 'Programming', 'Films', 'Games']
        };
    };
    ProfileService.prototype.getUserProfileMock = function (userId) {
        return {
            id: 'fjkhgjfkhgfldkjdfd',
            avatarUrl: "https://scontent-vie1-1.xx.fbcdn.net/v/t1.0-9/13118962_987362174684686_2846474144423139514_n.jpg?oh=8e79d34adfcb8ce5cbc8fb45c541c63e&oe=580C28EE",
            email: 'lol@mailinator.com',
            age: '23',
            contacts: '120',
            firstName: 'Hristo',
            lastName: 'Kolev',
            workplace: 'Casualino JSC',
            job: 'Front-end developer',
            interests: ['Sport', 'Programming', 'Films', 'Games']
        };
    };
    ProfileService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], ProfileService);
    return ProfileService;
}());
exports.ProfileService = ProfileService;
//# sourceMappingURL=profile-service.js.map