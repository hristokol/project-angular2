import {Component,OnInit,NgZone} from '@angular/core';
import {RouteParams} from '@angular/router-deprecated';
import {Observable} from 'rxjs/Observable';
import {ProfileService} from "../profile/components/profile-service";
import {PeerService} from "../peer/peer-service";
//import './main.js';

@Component({
    selector: 'call',
    templateUrl: 'app/call/views/call.html',
    providers: [ProfileService, PeerService],
    styleUrls: ['dist/build/call/call.css']
})

export class CallCmp implements OnInit {
    private _profileService:ProfileService;
    private peerService:PeerService;
    private _routeParams:RouteParams;
    private _zone:NgZone;

    public user:any;
    public cameraEnabled:boolean = false;
    public microphoneEnabled:boolean = true;
    public ongoingConversation:boolean = false;
    public time = 0;

    private seconds:number = 0;
    //noinspection TypeScriptUnresolvedFunction
    public timer = Observable.timer(0, 1000);

    constructor(zone:NgZone, routeParams:RouteParams, profileService:ProfileService, peerService:PeerService) {
        this._zone = zone;
        this._routeParams = routeParams;
        this._profileService = profileService;
        this.peerService = peerService;
    }

    ngOnInit() {
        this._load();
        console.log(this.user);
        console.log(this._routeParams.get('userId'));
        this.timer.subscribe(()=> {
            this._zone.run(()=> {
                this.func();
            })
        });
        this.peerService.call('ddd');
        this.toggleVideoCall();
    }

    private func() {
        this.time = new Date(1970, 0, 1).setSeconds(this.seconds++);
        console.log(this.time);
    }

    private _load() {
        this.user = this._profileService.getUserProfileMock(this._routeParams.get('userId'));
    }

    public toggleCamera() {
        this.cameraEnabled = !this.cameraEnabled;
    }

    public toggleMicrophone() {
        this.microphoneEnabled = !this.microphoneEnabled;
    }

    public toggleVideoCall() {
        (<any>navigator).getUserMedia = (<any>navigator).getUserMedia ||
            (<any>navigator).webkitGetUserMedia || (<any>navigator).mozGetUserMedia;

        var constraints = {
            audio: false,
            video: true
        };
        var video = document.querySelector('video');

        function successCallback(stream) {
            (<any>window).stream = stream; // stream available to console
            if (window.URL) {
                (<HTMLVideoElement>video).src = window.URL.createObjectURL(stream);
            } else {
                (<HTMLVideoElement>video).src = stream;
            }
        }

        function errorCallback(error) {
            console.log('navigator.getUserMedia error: ', error);
        }

        (<any>navigator).getUserMedia(constraints, successCallback, errorCallback);
    }

    public call() {

    }
}