"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var Observable_1 = require('rxjs/Observable');
var profile_service_1 = require("../profile/components/profile-service");
var peer_service_1 = require("../peer/peer-service");
//import './main.js';
var CallCmp = (function () {
    function CallCmp(zone, routeParams, profileService, peerService) {
        this.cameraEnabled = false;
        this.microphoneEnabled = true;
        this.ongoingConversation = false;
        this.time = 0;
        this.seconds = 0;
        //noinspection TypeScriptUnresolvedFunction
        this.timer = Observable_1.Observable.timer(0, 1000);
        this._zone = zone;
        this._routeParams = routeParams;
        this._profileService = profileService;
        this.peerService = peerService;
    }
    CallCmp.prototype.ngOnInit = function () {
        var _this = this;
        this._load();
        console.log(this.user);
        console.log(this._routeParams.get('userId'));
        this.timer.subscribe(function () {
            _this._zone.run(function () {
                _this.func();
            });
        });
        this.peerService.call('ddd');
        this.toggleVideoCall();
    };
    CallCmp.prototype.func = function () {
        this.time = new Date(1970, 0, 1).setSeconds(this.seconds++);
        console.log(this.time);
    };
    CallCmp.prototype._load = function () {
        this.user = this._profileService.getUserProfileMock(this._routeParams.get('userId'));
    };
    CallCmp.prototype.toggleCamera = function () {
        this.cameraEnabled = !this.cameraEnabled;
    };
    CallCmp.prototype.toggleMicrophone = function () {
        this.microphoneEnabled = !this.microphoneEnabled;
    };
    CallCmp.prototype.toggleVideoCall = function () {
        navigator.getUserMedia = navigator.getUserMedia ||
            navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
        var constraints = {
            audio: false,
            video: true
        };
        var video = document.querySelector('video');
        function successCallback(stream) {
            window.stream = stream; // stream available to console
            if (window.URL) {
                video.src = window.URL.createObjectURL(stream);
            }
            else {
                video.src = stream;
            }
        }
        function errorCallback(error) {
            console.log('navigator.getUserMedia error: ', error);
        }
        navigator.getUserMedia(constraints, successCallback, errorCallback);
    };
    CallCmp.prototype.call = function () {
    };
    CallCmp = __decorate([
        core_1.Component({
            selector: 'call',
            templateUrl: 'app/call/views/call.html',
            providers: [profile_service_1.ProfileService, peer_service_1.PeerService],
            styleUrls: ['dist/build/call/call.css']
        }), 
        __metadata('design:paramtypes', [core_1.NgZone, router_deprecated_1.RouteParams, profile_service_1.ProfileService, peer_service_1.PeerService])
    ], CallCmp);
    return CallCmp;
}());
exports.CallCmp = CallCmp;
//# sourceMappingURL=call.component.js.map