'use strict';
var Loader;
(function (Loader_1) {
    var Loader = (function () {
        function Loader() {
            var _this = this;
            this.loadScriptAsync = function (scriptSrc) {
                _this.items[scriptSrc] = { loaded: false, error: false };
                var element = document.createElement('script');
                element.src = scriptSrc;
                document.body.appendChild(element);
                element.onload = function () {
                    _this.update(scriptSrc, true);
                };
                element.onerror = function () {
                    _this.update(scriptSrc, false);
                };
                return _this;
            };
            this.loadStyleAsync = function (styleUrl) {
                _this.items[styleUrl] = { loaded: false, error: false };
                var element = document.createElement('link');
                element.rel = 'stylesheet';
                element.type = 'text/css';
                element.href = styleUrl;
                element.onload = function () {
                    _this.update(styleUrl, true);
                };
                element.onerror = function () {
                    _this.update(styleUrl, false);
                };
                document.body.appendChild(element);
                return _this;
            };
            this.update = function (item, loaded) {
                _this.items[item].loaded = loaded;
                _this.items[item].error = !loaded;
            };
            this.allLoaded = function () {
                var allLoaded = true;
                Object.keys(_this.items).forEach(function (item) {
                    if (_this.items[item].loaded == false) {
                        allLoaded = false;
                        return;
                    }
                });
                return allLoaded;
            };
            this.loadAllAsync = function () {
                _this.loadScriptAsync("node_modules/es6-shim/es6-shim.min.js")
                    .loadScriptAsync("node_modules/systemjs/dist/system-polyfills.js")
                    .loadScriptAsync("node_modules/angular2/es6/dev/src/testing/shims_for_IE.js")
                    .loadScriptAsync("node_modules/angular2/bundles/angular2-polyfills.js")
                    .loadScriptAsync("node_modules/systemjs/dist/system.src.js")
                    .loadScriptAsync("node_modules/rxjs/bundles/Rx.js")
                    .loadScriptAsync("node_modules/angular2/bundles/angular2.dev.js")
                    .loadScriptAsync("node_modules/angular2/bundles/router.dev.js")
                    .loadScriptAsync("node_modules/fastclick/lib/fastclick.js")
                    .loadScriptAsync("node_modules/angular2/bundles/http.dev.js");
                return {
                    then: function (callback) {
                        var interval = setInterval(function () {
                            if (_this.allLoaded() == true) {
                                clearInterval(interval);
                                callback();
                            }
                        });
                    }
                };
            };
            this.items = [];
        }
        return Loader;
    }());
    Loader_1.Loader = Loader;
})(Loader || (Loader = {}));
//# sourceMappingURL=loader.js.map