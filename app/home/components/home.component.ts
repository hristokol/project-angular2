import {Component} from '@angular/core';
import {NavigationCmp} from "../../navigation/components/navigation.component";
import {ROUTER_DIRECTIVES,RouteConfig} from '@angular/router-deprecated'
import {ContactsCmp} from "../../home/components/contacts/contacts.component";
import {CallsCmp} from "../../home/components/calls/calls.component";
import {ConversationsCmp} from "../../home/components/conversations/conversations.component";
import {LeftMenuCmp} from "../../leftMenu/components/left-menu.component";

@Component({
    selector: 'home',
    templateUrl: 'app/home/views/home.html',
    directives: [ROUTER_DIRECTIVES,NavigationCmp,LeftMenuCmp],
    styleUrls: ['dist/build/home/home.css']
})

@RouteConfig([
    {path:'/contacts', component: ContactsCmp, name: 'Contacts',useAsDefault: true},
    {path: '/calls', component: CallsCmp, name: 'Calls'},
    {path: '/conversations', component: ConversationsCmp, name:'Conversations'}
])

export class HomeCmp {}