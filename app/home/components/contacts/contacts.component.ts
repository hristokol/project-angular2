import {Component,OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, RouterOutlet} from '@angular/router-deprecated';
import {ContactsService} from "./contacts-service";

@Component({
    selector: 'contacts',
    providers: [ContactsService],
    directives: [ROUTER_DIRECTIVES, RouterOutlet],
    templateUrl: 'app/home/components/contacts/views/contacts.html',
    styleUrls: ['dist/build/home/components/contacts.css']
})

export class ContactsCmp implements OnInit {

    private _contactsService:ContactsService;
    public contacts:any[];

    constructor(contactsService:ContactsService) {
        this._contactsService = contactsService;
    }

    ngOnInit():any {
        this.load();
    }

    private load() {
        this.contacts = this._contactsService.loadMock();
        console.log(this.contacts)
    }
}

