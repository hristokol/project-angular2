"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
var ContactsService = (function () {
    function ContactsService() {
        this.http = http_1.Http;
    }
    ContactsService.prototype.load = function () {
        return this.http.get('contacts')
            .map(this.toJson)
            .catch(this.onError);
    };
    ContactsService.prototype.toJson = function (response) {
        var body = response.json();
        return body.data || {};
    };
    ContactsService.prototype.onError = function (error) {
        return Observable_1.Observable.throw(error);
    };
    ContactsService.prototype.loadMock = function () {
        return [
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378eue1',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Христо',
                lastName: 'Колев',
                id: 'uueuiwhfew783ofhn378eue2',
                online: true
            },
            {
                avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378eue3',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName: 'Kolevjkfhgfuhgjfdfdjkfd',
                id: 'uueuiwhfew783ofhn378euew4',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew5',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew6',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew7',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew8',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName: 'Kolevlast',
                id: 'uueuiwhfew783ofhn378euew9',
                online: true
            }
        ];
    };
    ContactsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], ContactsService);
    return ContactsService;
}());
exports.ContactsService = ContactsService;
//# sourceMappingURL=contacts-service.js.map