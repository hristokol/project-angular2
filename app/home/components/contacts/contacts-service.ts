import {Injectable} from '@angular/core';
import {Http,Response} from '@angular/http';

import {Observable} from 'rxjs/Observable';

@Injectable()

export class ContactsService {

    private http;

    constructor() {
        this.http = Http;
    }

    public load():Observable<any> {
        return this.http.get('contacts')
            .map(this.toJson)
            .catch(this.onError);
    }

    private toJson(response:Response) {
        let body = response.json();
        return body.data || {};
    }

    private onError(error:any) {
        return Observable.throw(error);
    }

    public loadMock() {
        return [
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName:'Kolev',
                id: 'uueuiwhfew783ofhn378euew',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName:'Kolev',
                id: 'uueuiwhfew783ofhn378eue1',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Христо',
                lastName:'Колев',
                id: 'uueuiwhfew783ofhn378eue2',
                online: true
            },
            {
                avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                firstName: 'Hristo',
                lastName:'Kolev',
                id: 'uueuiwhfew783ofhn378eue3',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName:'Kolevjkfhgfuhgjfdfdjkfd',
                id: 'uueuiwhfew783ofhn378euew4',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName:'Kolev',
                id: 'uueuiwhfew783ofhn378euew5',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName:'Kolev',
                id: 'uueuiwhfew783ofhn378euew6',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName:'Kolev',
                id: 'uueuiwhfew783ofhn378euew7',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName:'Kolev',
                id: 'uueuiwhfew783ofhn378euew8',
                online: true
            },
            {
                avatarUrl: 'https://mvpperformance.files.wordpress.com/2011/10/ferrari-prancing-horse.jpg',
                firstName: 'Hristo',
                lastName:'Kolevlast',
                id: 'uueuiwhfew783ofhn378euew9',
                online: true
            }
        ];
    }
}
