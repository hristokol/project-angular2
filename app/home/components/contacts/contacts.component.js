"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var contacts_service_1 = require("./contacts-service");
var ContactsCmp = (function () {
    function ContactsCmp(contactsService) {
        this._contactsService = contactsService;
    }
    ContactsCmp.prototype.ngOnInit = function () {
        this.load();
    };
    ContactsCmp.prototype.load = function () {
        this.contacts = this._contactsService.loadMock();
        console.log(this.contacts);
    };
    ContactsCmp = __decorate([
        core_1.Component({
            selector: 'contacts',
            providers: [contacts_service_1.ContactsService],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, router_deprecated_1.RouterOutlet],
            templateUrl: 'app/home/components/contacts/views/contacts.html',
            styleUrls: ['dist/build/home/components/contacts.css']
        }), 
        __metadata('design:paramtypes', [contacts_service_1.ContactsService])
    ], ContactsCmp);
    return ContactsCmp;
}());
exports.ContactsCmp = ContactsCmp;
//# sourceMappingURL=contacts.component.js.map