"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var navigation_component_1 = require("../../navigation/components/navigation.component");
var router_deprecated_1 = require('@angular/router-deprecated');
var contacts_component_1 = require("../../home/components/contacts/contacts.component");
var calls_component_1 = require("../../home/components/calls/calls.component");
var conversations_component_1 = require("../../home/components/conversations/conversations.component");
var left_menu_component_1 = require("../../leftMenu/components/left-menu.component");
var HomeCmp = (function () {
    function HomeCmp() {
    }
    HomeCmp = __decorate([
        core_1.Component({
            selector: 'home',
            templateUrl: 'app/home/views/home.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, navigation_component_1.NavigationCmp, left_menu_component_1.LeftMenuCmp],
            styleUrls: ['dist/build/home/home.css']
        }),
        router_deprecated_1.RouteConfig([
            { path: '/contacts', component: contacts_component_1.ContactsCmp, name: 'Contacts', useAsDefault: true },
            { path: '/calls', component: calls_component_1.CallsCmp, name: 'Calls' },
            { path: '/conversations', component: conversations_component_1.ConversationsCmp, name: 'Conversations' }
        ]), 
        __metadata('design:paramtypes', [])
    ], HomeCmp);
    return HomeCmp;
}());
exports.HomeCmp = HomeCmp;
//# sourceMappingURL=home.component.js.map