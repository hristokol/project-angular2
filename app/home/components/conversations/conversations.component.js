"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var conversations_service_1 = require("./conversations-service");
var ConversationsCmp = (function () {
    function ConversationsCmp(conversationsService) {
        this._conversationsService = conversationsService;
    }
    ConversationsCmp.prototype.ngOnInit = function () {
        this._load();
    };
    ConversationsCmp.prototype._load = function () {
        this.conversations = this._conversationsService.loadMock();
        console.log(this.conversations);
    };
    ConversationsCmp = __decorate([
        core_1.Component({
            selector: 'conversations',
            providers: [conversations_service_1.ConversationsService],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, router_deprecated_1.RouterLink, router_deprecated_1.RouterOutlet],
            templateUrl: 'app/home/components/conversations/views/conversations.html',
            styleUrls: ['dist/build/home/components/conversations.css']
        }), 
        __metadata('design:paramtypes', [conversations_service_1.ConversationsService])
    ], ConversationsCmp);
    return ConversationsCmp;
}());
exports.ConversationsCmp = ConversationsCmp;
//# sourceMappingURL=conversations.component.js.map