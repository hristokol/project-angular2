"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ConversationsService = (function () {
    function ConversationsService() {
    }
    ConversationsService.prototype.load = function () {
    };
    ConversationsService.prototype.loadMock = function () {
        return [
            {
                user: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Hristo',
                    lastName: 'Kolev',
                    id: 'uueuiwhfew783ofhn378euew0',
                    online: true
                },
                lastMessage: 'Hello how are you?',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Ivan',
                    lastName: 'Ivanov',
                    id: 'uueuiwhfew783ofhn378euew1',
                    online: false
                },
                lastMessage: 'Yodjkfdjk dsjnfsdjklnflkds sdkj fndsjknfkjl sdsdkjnfsdkljfjklsdf sdfdskjfsdkl; jfsd',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1ORu7fSeso0NBWyONbHuuZLb4J4zbRdImMlKBgJ7NViEXW0Xm',
                    firstName: 'Petar',
                    lastName: 'Stoyanov',
                    id: 'uueuiwhfew783ofhn378euew2',
                    online: true
                },
                lastMessage: 'Yolo?',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'https://upload.wikimedia.org/wikipedia/commons/0/07/Avatar_girl_face.png',
                    firstName: 'Banana',
                    lastName: 'Baanana',
                    id: 'uueuiwhfew783ofhn378euew3',
                    online: false
                },
                lastMessage: 'Cool :D',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Darth',
                    lastName: 'Vader',
                    id: 'uueuiwhfew783ofhn378euew4',
                    online: true
                },
                lastMessage: 'Heavy Breathing...',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: '50',
                    lastName: 'Cent',
                    id: 'uueuiwhfew783ofhn378euew5',
                    online: true
                },
                lastMessage: 'Hey, you in the club?',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'VeryLongFirstNameHaha',
                    lastName: 'Hhohohhdgfdfsdhfjkdshfkjsdfhsd',
                    id: 'uueuiwhfew783ofhn378euew6',
                    online: false
                },
                lastMessage: 'btw the message is very long too djkjdksnfsdjknfjkdsnfjksdnjfksd',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Hristo1',
                    lastName: 'VeryLongLastNameHaha',
                    id: 'uueuiwhfew783ofhn378euew7',
                    online: true
                },
                lastMessage: 'hello',
                timestamp: new Date().getTime(),
            }
        ];
    };
    ConversationsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], ConversationsService);
    return ConversationsService;
}());
exports.ConversationsService = ConversationsService;
//# sourceMappingURL=conversations-service.js.map