import {Component,OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES,RouterLink,RouterOutlet} from '@angular/router-deprecated';
import {ConversationsService} from "./conversations-service";

@Component({
    selector: 'conversations',
    providers: [ConversationsService],
    directives: [ROUTER_DIRECTIVES, RouterLink, RouterOutlet],
    templateUrl: 'app/home/components/conversations/views/conversations.html',
    styleUrls: ['dist/build/home/components/conversations.css']
})

export class ConversationsCmp implements OnInit {

    private _conversationsService:ConversationsService;
    public conversations:any[];

    constructor(conversationsService:ConversationsService) {
        this._conversationsService = conversationsService;
    }

    ngOnInit():any {
        this._load();
    }

    private _load() {
        this.conversations = this._conversationsService.loadMock();
        console.log(this.conversations);
    }
}