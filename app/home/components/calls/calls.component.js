"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var calls_service_1 = require("./calls-service");
var CallsCmp = (function () {
    function CallsCmp(callsService) {
        this._callsService = callsService;
    }
    CallsCmp.prototype.ngOnInit = function () {
        this._load();
    };
    CallsCmp.prototype._load = function () {
        this.calls = this._callsService.loadMock();
        console.log(this.calls);
    };
    CallsCmp = __decorate([
        core_1.Component({
            selector: 'calls',
            providers: [calls_service_1.CallsService],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, router_deprecated_1.RouterOutlet],
            templateUrl: 'app/home/components/calls/views/calls.html',
            styleUrls: ['dist/build/home/components/calls.css']
        }), 
        __metadata('design:paramtypes', [calls_service_1.CallsService])
    ], CallsCmp);
    return CallsCmp;
}());
exports.CallsCmp = CallsCmp;
//# sourceMappingURL=calls.component.js.map