import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, RouterOutlet} from '@angular/router-deprecated';
import {CallsService} from "./calls-service";

@Component({
    selector: 'calls',
    providers: [CallsService],
    directives: [ROUTER_DIRECTIVES, RouterOutlet],
    templateUrl: 'app/home/components/calls/views/calls.html',
    styleUrls: ['dist/build/home/components/calls.css']
})

export class CallsCmp implements OnInit {

    private _callsService:CallsService;
    public calls:any[];

    constructor(callsService:CallsService) {
        this._callsService = callsService;
    }

    ngOnInit():any {
        this._load();
    }

    private _load() {
        this.calls = this._callsService.loadMock();
        console.log(this.calls);
    }

}