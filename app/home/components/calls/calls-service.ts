import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';

@Injectable()
export class CallsService {

    constructor() {

    }

    public load() {
    }

    public loadMock() {
        return [
            {
                user: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Hristo',
                    lastName: 'Kolev',
                    id: 'uueuiwhfew783ofhn378euew0',
                    online: true
                },
                result: 'missed',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Trololol',
                    lastName: 'FragFrag',
                    id: 'uueuiwhfew783ofhn378euew1',
                    online: false
                },
                result: 'recieved',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1ORu7fSeso0NBWyONbHuuZLb4J4zbRdImMlKBgJ7NViEXW0Xm',
                    firstName: 'Yolo',
                    lastName: 'Yolo',
                    id: 'uueuiwhfew783ofhn378euew2',
                    online: true
                },
                result: 'outgoing',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'https://upload.wikimedia.org/wikipedia/commons/0/07/Avatar_girl_face.png',
                    firstName: 'Banana',
                    lastName: 'Baanana',
                    id: 'uueuiwhfew783ofhn378euew3',
                    online: false
                },
                result: 'recieved',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Darth',
                    lastName: 'Vader',
                    id: 'uueuiwhfew783ofhn378euew4',
                    online: true
                },
                result: 'recieved',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: '50',
                    lastName: 'Cent',
                    id: 'uueuiwhfew783ofhn378euew5',
                    online: true
                },
                result: 'recieved',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'VeryLongFirstNameHaha',
                    lastName: 'Hhohohhdgfdfsdhfjkdshfkjsdfhsd',
                    id: 'uueuiwhfew783ofhn378euew6',
                    online: false
                },
                result: 'missed',
                timestamp: new Date().getTime(),
            },
            {
                user: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Hristo1',
                    lastName: 'VeryLongLastNameHaha',
                    id: 'uueuiwhfew783ofhn378euew7',
                    online: true
                },
                result: 'missed',
                timestamp: new Date().getTime(),
            }
        ];
    }
}