import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES,RouterOutlet} from '@angular/router-deprecated';
import {EventService} from "../../navigation/components/event-service";
import {OptionsCmp} from "../../options/options.component";


@Component({
    selector: 'left-menu',
    templateUrl: 'app/leftMenu/views/leftMenu.html',
    styleUrls: ['dist/build/leftMenu/leftMenu.css'],
    directives: [ROUTER_DIRECTIVES, RouterOutlet, OptionsCmp]
})

export class LeftMenuCmp implements OnInit {
    private eventService:EventService;
    public tab:string;
    public opened:boolean;

    constructor(eventService:EventService) {
        this.eventService = eventService;
        this.opened = false;
    }

    public toggle(value:boolean):void {
        this.opened = value;
        this.tab = '';
    }

    public overlayClicked() {
        this.toggle(false);
    }

    public selectTab(tab:string):void {
        this.tab = tab;
    }

    public backCallback() {
        this.selectTab('left');
    }

    ngOnInit() {
        this.eventService.leftMenuOpened.subscribe((value)=>this.opened = value);
        this.backCallback = this.selectTab.bind(this);
    }
}