"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var event_service_1 = require("../../navigation/components/event-service");
var options_component_1 = require("../../options/options.component");
var LeftMenuCmp = (function () {
    function LeftMenuCmp(eventService) {
        this.eventService = eventService;
        this.opened = false;
    }
    LeftMenuCmp.prototype.toggle = function (value) {
        this.opened = value;
        this.tab = '';
    };
    LeftMenuCmp.prototype.overlayClicked = function () {
        this.toggle(false);
    };
    LeftMenuCmp.prototype.selectTab = function (tab) {
        this.tab = tab;
    };
    LeftMenuCmp.prototype.backCallback = function () {
        this.selectTab('left');
    };
    LeftMenuCmp.prototype.ngOnInit = function () {
        var _this = this;
        this.eventService.leftMenuOpened.subscribe(function (value) { return _this.opened = value; });
        this.backCallback = this.selectTab.bind(this);
    };
    LeftMenuCmp = __decorate([
        core_1.Component({
            selector: 'left-menu',
            templateUrl: 'app/leftMenu/views/leftMenu.html',
            styleUrls: ['dist/build/leftMenu/leftMenu.css'],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, router_deprecated_1.RouterOutlet, options_component_1.OptionsCmp]
        }), 
        __metadata('design:paramtypes', [event_service_1.EventService])
    ], LeftMenuCmp);
    return LeftMenuCmp;
}());
exports.LeftMenuCmp = LeftMenuCmp;
//# sourceMappingURL=left-menu.component.js.map