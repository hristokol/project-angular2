"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
require('./rxjs-imports');
var login_component_1 = require("./login/components/login.component");
var register_component_1 = require("./register/register.component");
var home_component_1 = require("./home/components/home.component");
var profile_component_1 = require("./profile/components/profile.component");
var profile_edit_component_1 = require("./profile/components/profile-edit.component");
var call_component_1 = require("./call/call.component");
var socket_service_1 = require("./socket/socket-service");
var event_service_1 = require("./navigation/components/event-service");
var chat_component_1 = require("./chat/chat.component");
var AppComponent = (function () {
    function AppComponent(socketService) {
        this._socketService = socketService;
    }
    AppComponent.prototype.ngOnInit = function () {
        //this._socketService.init();
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app',
            templateUrl: './app/app.html',
            styleUrls: ['dist/build/app/app.css'],
            providers: [socket_service_1.SocketService, event_service_1.EventService],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES]
        }),
        router_deprecated_1.RouteConfig([
            { path: '/login', component: login_component_1.LoginCmp, name: 'Login' },
            { path: '/register', component: register_component_1.RegisterCmp, name: 'Register' },
            { path: '/home/...', component: home_component_1.HomeCmp, name: 'Home', useAsDefault: true },
            { path: '/profile', component: profile_component_1.ProfileCmp, name: 'Profile' },
            { path: '/profile/edit', component: profile_edit_component_1.EditProfileCmp, name: 'Edit Profile' },
            { path: '/call/:userId', component: call_component_1.CallCmp, name: 'Call' },
            { path: '/chat/:userId', component: chat_component_1.ChatCmp, name: 'Chat' },
            { path: '/**', redirectTo: ['Login'] }
        ]), 
        __metadata('design:paramtypes', [socket_service_1.SocketService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=appComponent.js.map