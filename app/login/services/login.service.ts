import {Injectable} from '@angular/core';
import {Http,Response,Headers,RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class LoginService {
    private http:Http;

    constructor(http:Http) {
        this.http = http;
    }

    public login(username:string, password:string):Observable<any> {
        let body:string = JSON.stringify({username, password});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('https://127.0.0.1:3000/login', body, options)
            .map(this.extractData)
            .catch(this.onError);
    }

    private extractData(response:Response) {
        let body = response.json();
        return body || {};
    }

    private onError(error:any) {
        let errMsg:string = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }


}