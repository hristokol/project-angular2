import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES,RouterOutlet, Router} from '@angular/router-deprecated';
import {FormBuilder, ControlGroup} from '@angular/common';
import {LoginService} from "../services/login.service";

@Component({
    selector: 'login',
    templateUrl: 'app/login/views/login.html',
    styleUrls: ['dist/build/login/login.css'],
    providers: [LoginService],
    directives: [ROUTER_DIRECTIVES, RouterOutlet]
})

export class LoginCmp implements OnInit {
    private loginService:LoginService;
    private formBuilder:FormBuilder;
    private router:Router;

    public form:ControlGroup;

    constructor(loginService:LoginService, formBuilder:FormBuilder, router:Router) {
        this.loginService = loginService;
        this.formBuilder = formBuilder;
        this.router = router;
    }

    ngOnInit():any {
        this.initForm();
    }

    private initForm() {
        this.form = this.formBuilder.group({
            username: this.formBuilder.control(''),
            password: this.formBuilder.control('')
        });
    }

    public login() {
        this.loginService.login(this.form.value.username, this.form.value.password)
            .subscribe(this.onSuccessLogin, this.onError);
    }

    private onSuccessLogin = (response:any) => {
        console.log(response);
        if (response.success) {
            this.router.navigate(['Home/Contacts']);
        }
    }

    private onError(error:any) {

    }


}