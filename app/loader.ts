'use strict'

module Loader {

    interface IItem {
        loaded:boolean,
        error:boolean
    }

    interface IItems {
        [k:string] : IItem;
    }

    export class Loader {

        private items:Array<IItems>;

        constructor() {
            this.items = [];
        }

        private loadScriptAsync = (scriptSrc:string)=> {
            this.items[scriptSrc] = {loaded: false, error: false};

            let element = document.createElement('script');
            element.src = scriptSrc;
            document.body.appendChild(element);

            element.onload = ()=> {
                this.update(scriptSrc, true);
            };

            element.onerror = ()=> {
                this.update(scriptSrc, false);
            };

            return this;
        };

        private loadStyleAsync = (styleUrl:string)=> {
            this.items[styleUrl] = {loaded: false, error: false};
            let element = document.createElement('link');
            element.rel = 'stylesheet';
            element.type = 'text/css';
            element.href = styleUrl;

            element.onload = ()=> {
                this.update(styleUrl, true);
            };

            element.onerror = ()=> {
                this.update(styleUrl, false);
            };

            document.body.appendChild(element);
            return this;
        };

        private update = (item:string, loaded:boolean)=> {
            this.items[item].loaded = loaded;
            this.items[item].error = !loaded;
        };

        private allLoaded = ():boolean=> {
            let allLoaded = true;
            Object.keys(this.items).forEach((item:string)=> {
                if (this.items[item].loaded == false) {
                    allLoaded = false;
                    return;
                }
            });

            return allLoaded;
        };

        public loadAllAsync = ()=> {
            this.loadScriptAsync("node_modules/es6-shim/es6-shim.min.js")
                .loadScriptAsync("node_modules/systemjs/dist/system-polyfills.js")
                .loadScriptAsync("node_modules/angular2/es6/dev/src/testing/shims_for_IE.js")
                .loadScriptAsync("node_modules/angular2/bundles/angular2-polyfills.js")
                .loadScriptAsync("node_modules/systemjs/dist/system.src.js")
                .loadScriptAsync("node_modules/rxjs/bundles/Rx.js")
                .loadScriptAsync("node_modules/angular2/bundles/angular2.dev.js")
                .loadScriptAsync("node_modules/angular2/bundles/router.dev.js")
                .loadScriptAsync("node_modules/fastclick/lib/fastclick.js")
                .loadScriptAsync("node_modules/angular2/bundles/http.dev.js");

            return {
                then: (callback)=> {
                    let interval = setInterval(()=> {
                        if (this.allLoaded() == true) {
                            clearInterval(interval);
                            callback();
                        }
                    });
                }
            }

        };

    }
}