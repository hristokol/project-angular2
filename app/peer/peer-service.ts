import {Injectable} from '@angular/core';
declare var peer;

@Injectable()
export class PeerService {
    private connection:any;

    constructor() {

    }

    public call(otherUserId:string) {
        this.connection = peer.connect(otherUserId);

        this.setListeners();
    }

    private setListeners() {
        this.connection.on('open', this.onConnectionOpened);
        peer.on('connection', this.onDataReceived);
    }

    private onConnectionOpened = ():void=> {
        this.connection.send('Hello');
    }

    private onDataReceived = (connection:any):void=> {
        connection.on('data', (data:any)=> {
            console.log(data);
        });
    }
}