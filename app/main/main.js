"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var http_1 = require('@angular/http');
var appComponent_1 = require('../appComponent');
core_1.enableProdMode();
platform_browser_dynamic_1.bootstrap(appComponent_1.AppComponent, [
    router_deprecated_1.ROUTER_PROVIDERS, http_1.HTTP_PROVIDERS
]);
//# sourceMappingURL=main.js.map