import {Injectable} from '@angular/core';
import {Http,Response,Headers,RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ChatService {
    private http:Http;

    constructor(http:Http) {
        this.http = http;
    }

    public load(otherUserId:string):Observable<any> {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get('https://127.0.0.1:3000/chat/'+otherUserId)
            .map(this.extractData)
            .catch(this.onError);
    }

    public send(message:string, receiverId:string):Observable<any> {
        let body:string = JSON.stringify({message});
        let headers:any = new Headers({'Content-type': 'application/json'});
        let options:any = new RequestOptions({headers: headers});

        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('https://127.0.0.1:3000/chat/' + receiverId, body, options)
            .catch(this.onError);
    }

    private extractData(response:Response) {
        let body = response.json();
        return body.data || {};
    }

    private onError(error:any) {
        return Observable.throw(error);
    }
}