"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var chat_service_1 = require("./chat-service");
var ChatCmp = (function () {
    function ChatCmp(routeParams, chatService) {
        var _this = this;
        this.onSendSuccess = function (response) {
            _this.message.text = '';
            _this.scrollToBottom();
        };
        this.onSendError = function (error) {
            _this.message.text = '';
            _this.scrollToBottom();
            console.error(error);
        };
        this.scrollToBottom = function () {
            try {
                _this.myScrollContainer.nativeElement.scrollTop = _this.myScrollContainer.nativeElement.scrollHeight;
            }
            catch (err) {
                console.error(err);
            }
        };
        this.routeParams = routeParams;
        this.otherUserId = this.routeParams.get('userId');
        this.chatService = chatService;
        this.message = { text: '' };
        this.messages = [];
    }
    ChatCmp.prototype.ngOnInit = function () {
        this.load();
    };
    ChatCmp.prototype.ngAfterViewInit = function () {
        this.scrollToBottom();
    };
    ChatCmp.prototype.load = function () {
        this.messages = [{
                sender: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Hristo',
                    lastName: 'Kolev',
                    id: 'uueuiwhfew783ofhn378euew0',
                    online: true
                },
                text: 'Hello',
                createdAt: Date.now()
            }, {
                sender: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Hristo',
                    lastName: 'Kolev',
                    id: 'uueuiwhfew783ofhn378euew0',
                    online: true
                },
                text: 'lol',
                createdAt: Date.now()
            }, {
                sender: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Hristo',
                    lastName: 'Kolev',
                    id: 'uueuiwhfew783ofhn378euew0',
                    online: true
                },
                text: ':D',
                createdAt: Date.now()
            }, {
                sender: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Hristo',
                    lastName: 'Kolev',
                    id: 'uueuiwhfew783ofhn378euew1',
                    online: true
                },
                text: 'mm',
                createdAt: Date.now()
            }, {
                sender: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Hristo',
                    lastName: 'Kolev',
                    id: 'uueuiwhfew783ofhn378euew0',
                    online: true
                },
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                createdAt: Date.now()
            }, {
                sender: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Hristo',
                    lastName: 'Kolev',
                    id: 'uueuiwhfew783ofhn378euew1',
                    online: true
                },
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                createdAt: Date.now()
            }, {
                sender: {
                    avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                    firstName: 'Hristo',
                    lastName: 'Kolev',
                    id: 'uueuiwhfew783ofhn378euew1',
                    online: true
                },
                text: ':D',
                createdAt: Date.now()
            }];
        this.scrollToBottom();
        this.chatService.load(this.otherUserId).subscribe(this.onLoadSuccess, this.onLoadError);
    };
    ChatCmp.prototype.onLoadSuccess = function (response) {
        this.messages = response;
    };
    ChatCmp.prototype.onLoadError = function (error) {
        console.error(error);
    };
    ChatCmp.prototype.send = function () {
        this.messages.push({
            sender: {
                avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew1',
                online: true
            },
            text: this.message.text,
            createdAt: Date.now()
        });
        this.scrollToBottom();
        this.chatService.send(this.message.text, 'gg').subscribe(this.onSendSuccess, this.onSendError);
    };
    __decorate([
        core_1.ViewChild('list'), 
        __metadata('design:type', core_1.ElementRef)
    ], ChatCmp.prototype, "myScrollContainer", void 0);
    ChatCmp = __decorate([
        core_1.Component({
            selector: 'chat-user',
            providers: [chat_service_1.ChatService],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, router_deprecated_1.RouterOutlet],
            templateUrl: 'app/chat/views/chat.html',
            styleUrls: ['dist/build/chat/chat.css']
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.RouteParams, chat_service_1.ChatService])
    ], ChatCmp);
    return ChatCmp;
}());
exports.ChatCmp = ChatCmp;
//# sourceMappingURL=chat.component.js.map