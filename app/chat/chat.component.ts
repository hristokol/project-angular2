import {Component, ViewChild,AfterViewInit,OnInit,ElementRef} from '@angular/core';
import {ROUTER_DIRECTIVES,RouteParams,RouterOutlet} from '@angular/router-deprecated';
import {ChatService} from "./chat-service";

@Component({
    selector: 'chat-user',
    providers: [ChatService],
    directives: [ROUTER_DIRECTIVES, RouterOutlet],
    templateUrl: 'app/chat/views/chat.html',
    styleUrls: ['dist/build/chat/chat.css']
})

export class ChatCmp implements OnInit,AfterViewInit {

    //noinspection TypeScriptValidateTypes
    @ViewChild('list') private myScrollContainer:ElementRef;
    private routeParams:RouteParams;
    private otherUserId:string;
    private chatService:ChatService;

    public message:any;
    public messages:any[];

    constructor(routeParams:RouteParams, chatService:ChatService) {
        this.routeParams = routeParams;
        this.otherUserId = this.routeParams.get('userId');
        this.chatService = chatService;
        this.message = {text: ''};
        this.messages = [];
    }

    ngOnInit():any {
        this.load();
    }

    ngAfterViewInit():any {
        this.scrollToBottom();
    }

    private load() {
        this.messages = [{
            sender: {
                avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew0',
                online: true
            },
            text: 'Hello',
            createdAt: Date.now()
        }, {
            sender: {
                avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew0',
                online: true
            },
            text: 'lol',
            createdAt: Date.now()
        }, {
            sender: {
                avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew0',
                online: true
            },
            text: ':D',
            createdAt: Date.now()
        }, {
            sender: {
                avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew1',
                online: true
            },
            text: 'mm',
            createdAt: Date.now()
        }, {
            sender: {
                avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew0',
                online: true
            },
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
            createdAt: Date.now()
        }, {
            sender: {
                avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew1',
                online: true
            },
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
            createdAt: Date.now()
        }, {
            sender: {
                avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew1',
                online: true
            },
            text: ':D',
            createdAt: Date.now()
        }];
        this.scrollToBottom();
        this.chatService.load(this.otherUserId).subscribe(this.onLoadSuccess, this.onLoadError);
    }

    private onLoadSuccess(response:any) {
        this.messages = response;
    }

    private onLoadError(error:any) {
        console.error(error);
    }

    public send() {
        this.messages.push({
            sender: {
                avatarUrl: 'http://thumb9.shutterstock.com/display_pic_with_logo/265489/265489,1294946094,2/stock-vector-cartoon-troll-holding-a-club-vector-illustration-with-no-gradients-all-in-a-single-layer-68916088.jpg',
                firstName: 'Hristo',
                lastName: 'Kolev',
                id: 'uueuiwhfew783ofhn378euew1',
                online: true
            },
            text: this.message.text,
            createdAt: Date.now()
        });
        this.scrollToBottom();
        this.chatService.send(this.message.text, 'gg').subscribe(this.onSendSuccess, this.onSendError);
    }

    private onSendSuccess = (response:any) => {
        this.message.text = '';
        this.scrollToBottom();
    }

    private onSendError = (error:any) => {
        this.message.text = '';
        this.scrollToBottom();
        console.error(error);
    }

    public scrollToBottom = ():void => {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch (err) {
            console.error(err);
        }
    }
}
