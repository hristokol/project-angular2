"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
var ChatService = (function () {
    function ChatService(http) {
        this.http = http;
    }
    ChatService.prototype.load = function (otherUserId) {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get('https://127.0.0.1:3000/chat/' + otherUserId)
            .map(this.extractData)
            .catch(this.onError);
    };
    ChatService.prototype.send = function (message, receiverId) {
        var body = JSON.stringify({ message: message });
        var headers = new http_1.Headers({ 'Content-type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('https://127.0.0.1:3000/chat/' + receiverId, body, options)
            .catch(this.onError);
    };
    ChatService.prototype.extractData = function (response) {
        var body = response.json();
        return body.data || {};
    };
    ChatService.prototype.onError = function (error) {
        return Observable_1.Observable.throw(error);
    };
    ChatService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ChatService);
    return ChatService;
}());
exports.ChatService = ChatService;
//# sourceMappingURL=chat-service.js.map