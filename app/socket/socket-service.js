"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var SocketService = (function () {
    function SocketService() {
    }
    Object.defineProperty(SocketService.prototype, "socket", {
        get: function () {
            return this._socket;
        },
        enumerable: true,
        configurable: true
    });
    SocketService.prototype.options = function () {
        return {
            'multiplex': false,
            'force new connection': true,
            'jsonp': false,
            'reconnectionDelay': 2000,
            'reconnectionDelayMax': 5000,
            'reconnectionAttempts': 5,
            'transports': ['websocket']
        };
    };
    SocketService.prototype.init = function () {
        //noinspection TypeScriptUnresolvedVariable
        this._socket = io.connect('https://127.0.0.1:3000' + '?token=fdfsd', this.options());
        this._socket.on('error', this.onError);
        this._socket.on('connect', this.onConnect);
        this._socket.on('connect_error', this.onConnectError);
        this._socket.on('disconnect', this.onDisconnect);
        this._socket.on('reconnect', this.onReconnect);
        this._socket.on('reconnect_attempt', this.onReconnectAttempt);
        this._socket.on('reconnecting', this.onReconnecting);
        this._socket.on('reconnect_failed', this.onReconnectFailed);
    };
    SocketService.prototype.onError = function (error) {
        console.log('Websocket error!', error);
    };
    SocketService.prototype.onConnect = function () {
        console.log('Websocket connected!');
    };
    SocketService.prototype.onConnectError = function (error) {
        console.log('Websocket connect error!', error);
    };
    SocketService.prototype.onDisconnect = function () {
        console.log('Websocket disconnected!');
    };
    SocketService.prototype.onReconnect = function () {
        console.log('Websocket reconnected!');
    };
    SocketService.prototype.onReconnectAttempt = function (attempt) {
        console.log('Websocket reconnect attempt:', attempt);
    };
    SocketService.prototype.onReconnecting = function () {
        console.log('Websocket reconnecting!');
    };
    SocketService.prototype.onReconnectFailed = function () {
        console.log('Websocket failed to reconnect!');
    };
    SocketService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], SocketService);
    return SocketService;
}());
exports.SocketService = SocketService;
//# sourceMappingURL=socket-service.js.map