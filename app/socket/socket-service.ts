import {Injectable} from '@angular/core';
declare var io;

@Injectable()
export class SocketService {
    private _socket;

    constructor() {
    }

    public get socket() {
        return this._socket;
    }

    private options():any {
        return {
            'multiplex': false,
            'force new connection': true,
            'jsonp': false,
            'reconnectionDelay': 2000,
            'reconnectionDelayMax': 5000,
            'reconnectionAttempts': 5,
            'transports': ['websocket']
        };
    }

    public init():void {
        //noinspection TypeScriptUnresolvedVariable
        this._socket = io.connect('https://127.0.0.1:3000' + '?token=fdfsd', this.options());
        this._socket.on('error', this.onError);
        this._socket.on('connect', this.onConnect);
        this._socket.on('connect_error', this.onConnectError);
        this._socket.on('disconnect', this.onDisconnect);
        this._socket.on('reconnect', this.onReconnect);
        this._socket.on('reconnect_attempt', this.onReconnectAttempt);
        this._socket.on('reconnecting', this.onReconnecting);
        this._socket.on('reconnect_failed', this.onReconnectFailed);
    }

    private onError(error):void {
        console.log('Websocket error!', error);
    }

    private onConnect():void {
        console.log('Websocket connected!');
    }

    private onConnectError(error):void {
        console.log('Websocket connect error!', error);
    }

    private onDisconnect():void {
        console.log('Websocket disconnected!');
    }

    private onReconnect():void {
        console.log('Websocket reconnected!');
    }

    private onReconnectAttempt(attempt):void {
        console.log('Websocket reconnect attempt:', attempt);
    }

    private onReconnecting():void {
        console.log('Websocket reconnecting!');
    }

    private onReconnectFailed():void {
        console.log('Websocket failed to reconnect!');
    }

}