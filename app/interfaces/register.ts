module Registration {
    export interface IRegisterForm {
        firstName: string,
        lastName: string,
        email: string ,
        password: string,
        rePassword:string
    }
}