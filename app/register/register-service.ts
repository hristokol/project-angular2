import {Injectable} from '@angular/core';
import {Http,Headers,RequestOptions,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import IRegisterForm = Registration.IRegisterForm;

@Injectable()
export class RegistrationService {
    private http:Http;

    constructor(http:Http) {
        this.http = http;
    }

    public register(form:IRegisterForm):Observable<any> {
        let body:string = JSON.stringify({form});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});

        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('https://127.0.0.1:3000/register', body, options)
            .map(this.extractData)
            .catch(this.onError);
    }

    private extractData(response:Response) {
        let body = response.json();
        return body|| {};
    }

    private onError(error:any) {
        let errMsg:string = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}