"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var register_service_1 = require("./register-service");
var router_deprecated_1 = require('@angular/router-deprecated');
var RegisterCmp = (function () {
    function RegisterCmp(formBuilder, router, registerService) {
        var _this = this;
        this.onSuccessRegister = function (response) {
            console.log(response);
            if (response.success) {
                _this.router.navigate(['Login']);
            }
        };
        this.formBuilder = formBuilder;
        this.router = router;
        this.registerService = registerService;
    }
    RegisterCmp.prototype.ngOnInit = function () {
        this.initForm();
    };
    RegisterCmp.prototype.initForm = function () {
        this.registerForm = this.formBuilder.group({
            email: this.formBuilder.control(''),
            firstName: this.formBuilder.control(''),
            lastName: this.formBuilder.control(''),
            password: this.formBuilder.control(''),
            repeatPassword: this.formBuilder.control('')
        });
    };
    RegisterCmp.prototype.register = function () {
        var form = {
            email: this.registerForm.value.email,
            firstName: this.registerForm.value.firstName,
            lastName: this.registerForm.value.lastName,
            password: this.registerForm.value.password,
            rePassword: this.registerForm.value.repeatPassword
        };
        this.registerService.register(form).subscribe(this.onSuccessRegister, this.onError);
    };
    RegisterCmp.prototype.onError = function (error) {
    };
    RegisterCmp = __decorate([
        core_1.Component({
            selector: 'register',
            templateUrl: 'app/register/views/register.html',
            styleUrls: ['dist/build/register/register.css'],
            providers: [register_service_1.RegistrationService]
        }), 
        __metadata('design:paramtypes', [common_1.FormBuilder, router_deprecated_1.Router, register_service_1.RegistrationService])
    ], RegisterCmp);
    return RegisterCmp;
}());
exports.RegisterCmp = RegisterCmp;
//# sourceMappingURL=register.component.js.map