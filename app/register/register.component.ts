import {Component, OnInit} from '@angular/core';
import {FormBuilder, ControlGroup} from '@angular/common';
import {RegistrationService} from "./register-service";
import {Router} from '@angular/router-deprecated';
import IRegisterForm = Registration.IRegisterForm;

@Component({
    selector: 'register',
    templateUrl: 'app/register/views/register.html',
    styleUrls: ['dist/build/register/register.css'],
    providers: [RegistrationService]
})

export class RegisterCmp implements OnInit {
    private formBuilder:FormBuilder;
    private router:Router;
    private registerService:RegistrationService;


    public registerForm:ControlGroup;

    constructor(formBuilder:FormBuilder, router:Router, registerService:RegistrationService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.registerService = registerService;
    }

    ngOnInit():any {
        this.initForm();
    }

    private initForm() {
        this.registerForm = this.formBuilder.group({
            email: this.formBuilder.control(''),
            firstName: this.formBuilder.control(''),
            lastName: this.formBuilder.control(''),
            password: this.formBuilder.control(''),
            repeatPassword: this.formBuilder.control('')
        });
    }

    public register() {
        let form:IRegisterForm = {
            email: this.registerForm.value.email,
            firstName: this.registerForm.value.firstName,
            lastName: this.registerForm.value.lastName,
            password: this.registerForm.value.password,
            rePassword: this.registerForm.value.repeatPassword
        };
        this.registerService.register(form).subscribe(this.onSuccessRegister, this.onError);
    }

    private onSuccessRegister = (response:any) => {
        console.log(response);
        if (response.success) {
            this.router.navigate(['Login']);
        }
    }

    private onError(error:any) {

    }
}