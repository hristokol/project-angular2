import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES,RouterOutlet} from '@angular/router-deprecated';
import {EventService} from "./event-service";

@Component({
    selector: 'navigation',//<navigation></navigation>,
    templateUrl: 'app/navigation/components/views/navigation.html',
    styleUrls: ['dist/build/navigation/navigation.css'],
    directives: [ROUTER_DIRECTIVES, RouterOutlet]
})

export class NavigationCmp {
    private eventService:EventService;

    constructor(eventService:EventService) {
        this.eventService = eventService;
    }

    public toggleLeftMenu(value:boolean) {
        this.eventService.toggleLeftMenu(value);
    }

}