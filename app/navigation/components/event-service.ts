import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class EventService {

    private leftMenuSource:Subject<boolean>;
    public leftMenuOpened;

    constructor() {
        this.leftMenuSource = new Subject<boolean>();
        this.leftMenuOpened = this.leftMenuSource.asObservable();
    }

    public toggleLeftMenu(value:boolean) {
        this.leftMenuSource.next(value);
    }

}