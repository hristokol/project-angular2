import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES,RouteConfig} from '@angular/router-deprecated';
import {Http,Headers,RequestOptions,Response} from '@angular/http';

import './rxjs-imports';

import {LoginCmp} from "./login/components/login.component";
import {RegisterCmp} from "./register/register.component";
import {HomeCmp} from "./home/components/home.component";
import {ContactsService} from "./home/components/contacts/contacts-service";
import {ProfileCmp} from "./profile/components/profile.component";
import {EditProfileCmp} from "./profile/components/profile-edit.component";
import {CallCmp} from "./call/call.component";
import {SocketService} from "./socket/socket-service";
import {EventService} from "./navigation/components/event-service";
import {ChatCmp} from "./chat/chat.component";

@Component({
    selector: 'app',
    templateUrl: './app/app.html',
    styleUrls: ['dist/build/app/app.css'],
    providers: [SocketService, EventService],
    directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
    {path: '/login', component: LoginCmp, name: 'Login'},
    {path: '/register', component: RegisterCmp, name: 'Register'},
    {path: '/home/...', component: HomeCmp, name: 'Home', useAsDefault: true},
    {path: '/profile', component: ProfileCmp, name: 'Profile'},
    {path: '/profile/edit', component: EditProfileCmp, name: 'Edit Profile'},
    {path: '/call/:userId', component: CallCmp, name: 'Call'},
    {path: '/chat/:userId', component: ChatCmp, name: 'Chat'},
    {path: '/**', redirectTo: ['Login']}
])

export class AppComponent implements OnInit {
    private _socketService:SocketService;

    constructor(socketService:SocketService) {
        this._socketService = socketService;
    }

    ngOnInit():any {
        //this._socketService.init();
    }
}

